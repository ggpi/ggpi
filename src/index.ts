import { bus } from "./bus"
import { config } from "./config"
import { Point } from "./model"
import { log } from "./tool/log"

export { bus, config, Point, log }

(async function() {
    await config.load();

    require('./storage/sqlite');
    require('./protocol/avl8');

    require('./sensor/adc');
    require('./sensor/random');
    require('./sensor/disk');
    require('./sensor/adc');
    require('./sensor/gpio');
    require('./sensor/sysinfo');
    require('./sensor/temperature');

    require('./navigation/gps');

    bus.start();
})();
