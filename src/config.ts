export const config = new class Config {
    sendIntervalDrive = 10 * 1000
    sendIntervalStop = 15 * 60 * 1000
    sensors = {
        disk: {
            ioId: 1
        },
        dut: [] as config.DUT[],
        sys: {
            uptimeIoId: 11,
            laIoId: 12,
            ramIoId: 13,
        }
    }

    async load() {
    }

    async save() {
    }
}

export namespace config {
    export interface DUT {
    }
}
