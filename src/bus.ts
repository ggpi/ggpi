import { Point, config } from "."

export const bus = new class Bus {
    start() {
        setTimeout(() => this.collect("bus"), 0);
    }

    async collect(origin: string, initialPoint?: Partial<Point>) {
        // TODO: Debounce this call
        const point: Point = {
            blobs: {},
            gps: {},
            sensors: {
                num: {},
                bin: {},
            },
            events: [],
            ...initialPoint,
            createdAt: new Date(),
            origin: origin,
        }

        let handlers = this.collectHandlers;
        let pass = 0;
        do {
            const promises = handlers.map(handler => handler(point, pass));
            const rvs = await Promise.all(promises);
            handlers = rvs.reduce((filtered, rv, idx) => {
                if (rv === 'nextpass') filtered.push(handlers[idx]);
                return filtered;
            }, [] as bus.OnCollectHandler[]);
            pass++;
        } while (handlers.length > 0 && pass < 5);

        // TODO: Save
        // TODO: Send

        console.log('Processed new point', point);

        setTimeout(() => this.collect("bus"), config.sendIntervalDrive);
    }

    onCollect(handler: bus.OnCollectHandler) {
        this.collectHandlers.push(handler);
    }

    onSave(handler: bus.OnSaveHandler) {
    }

    onSend(handler: bus.OnSendHandler) {
    }

    private collectHandlers = [] as bus.OnCollectHandler[];
}

export namespace bus {
    export type OnCollectHandler = (point: Point, pass: number) => Promise<"nextpass" | void>;
    export type OnSaveHandler = (point: Point, pass: number) => Promise<"nextpass" | void>;
    export type OnSendHandler = (point: Point, pass: number) => Promise<"nextpass" | void>;
}
