import { bus, config, Point } from ".."

export class DUT {
    constructor(private config: config.DUT) {
        bus.onCollect(point=> this.onCollect(point));
    }

    private async onCollect(point: Point) {
    }
}

for (const conf of config.sensors.dut) {
    const sensor = new DUT(conf);
}
