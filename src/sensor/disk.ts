import { bus, config } from ".."
const diskspace = require('diskspace');

let prevSpace = 0;

bus.onCollect(async point => {
    return new Promise<void>(resolve => {
        diskspace.check('/', (err: any, rv: { free: number, total: number }) => {
            if (err) {
                console.error(err);
            }
            const free = Math.round((rv.free / rv.total) * 100);
            if (free !== prevSpace) {
                point.sensors.num[config.sensors.disk.ioId] = free;
                prevSpace = free;
            }
            resolve();
        });
    });
});
