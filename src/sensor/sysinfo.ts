import { bus, config, Point } from ".."
import * as os from "os"

let prevRAM = 0;
let prevLA = 0;
let prevUptime = 0;

bus.onCollect(async point => {
    prevRAM     = saveSensor(point, prevRAM, Math.round(os.freemem() / os.totalmem() * 100), config.sensors.sys.ramIoId);
    prevLA      = saveSensor(point, prevLA, os.loadavg()[0], config.sensors.sys.laIoId);
    prevUptime  = saveSensor(point, prevUptime, os.uptime(), config.sensors.sys.uptimeIoId);
});

function saveSensor(point: Point, prevVal: number, newVal: number, ioId: number) {
    if (newVal !== prevVal) {
        point.sensors.num[ioId] = newVal;
    }
    return newVal;
}
