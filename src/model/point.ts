export interface Position {
    lat?: number
    lng?: number
    speed?: number
    alt?: number
    sat?: number
    nmea?: string
}

export interface PointEvent {
}

export interface Point {
    createdAt: Date
    urgent?: boolean
    origin: string
    gps: Position
    glonass?: Position
    sensors: {
        num: {
            [key: number]: number
        }
        bin: {
            [key: number]: boolean
        }
    }
    blobs: {
    }
    events: PointEvent[]
}
